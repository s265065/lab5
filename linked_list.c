#include "linked_list.h"

#include <stdlib.h>


node* list_create(int number){
    node* new_list = malloc(sizeof(number));
    new_list->data = number;
    new_list->next = NULL;
    return new_list;
}

void list_add_front(int number, node ** list){
    if (!list) {
        return;
    }

    node* new_list = list_create(number);
    new_list->next = *list;
    *list = new_list;
}

void list_add_back(int value, node ** list) {
    node * last = NULL;
    node * current;

    if (!list) {
        return;
    }

    current = *list;
    while (current) {
        last = current;
        current = current-> next;
    }

    if (!last) {
        list_add_front(value, list);
        return;
    }

    last->next = list_create(value);
}

void list_free(node* list){
    node* for_deleting;
    while (list) {
        for_deleting = list;
        list = list->next;
        free(for_deleting);
    }
}

int list_length(const node* list){
    int length = 0;
    while (list) {
        list = list->next;
        ++length;
    }
    return length;
}

node* list_node_at(node* list, int index){

    while(index>0){
        if (list == NULL) {
            return NULL;
        }

        list = list->next;
        --index;
    }
    return list;
}

int list_get(const node* list, int index){
    const  node* index_node = list_node_at((node*) list, index);
    if (index_node!=NULL){
        return index_node->data;
    }
    return 0;

}

long int list_sum(const node* list){
    int sum = 0;
    while (list) {
        sum+=list->data;
        list = list->next;
    }
    return sum;
}

void list_foreach(const node * list, void (* f)(int)){
    while (list) {
        f(list->data);
        list=list->next;
    }
}

node * list_map(long int (* f)(int), const node * list){

    node * new_list = list_create(f(list->data));
    list=list->next;

    while (list){
        list_add_back(f(list->data),&new_list);
        list= list->next;
    }

    return new_list;
}

void list_map_mut(int (* f)(int), node * list){
    while (list) {
        list->data=f(list->data);
        list=list->next;
    }
}

long int list_foldl(int a,  long int (* f)(int, int), const node * list){
    while (list) {
        a = f(list->data, a);
        list=list->next;
    }
    return a;
}

node * list_iterate(int s, unsigned int n, long int (* f)(int)){
    int value = s;

    node * new_list = list_create(value);

    for (unsigned int i = 1; i < n; ++i) {
        value = f(value);
        list_add_back(value,&new_list);
    }

    return new_list;
}



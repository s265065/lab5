
#include <tiff.h>

typedef struct node{
    int data;
    struct node* next;
} node;

/*accepts a number, returns a pointer to the new linked list node.*/
node* list_create(int number);

/*accepts a number and a pointer to a pointer to the linked list. Prepends the new node with a number to the list.*/
void list_add_front(int number, node **list);

/*adds an element to the end of the list. The signature is the same as list_add_front.*/
void list_add_back(int number, node ** list);

/*frees the memory allocated to all elements of list.*/
void list_free(node* list);

/*accepts a list and computes its length.*/
int list_length(const node* list);

/*accepts a list and an index, returns a pointer to struct list, corresponding to the node at this index. If the index is too big, returns NULL.*/
node* list_node_at(node* list, int index);

/*gets an element by index, or returns 0 if the index is outside the list bounds.*/
int list_get(const node* list, int index);

/*accepts a list, returns the sum of elements.*/
long int list_sum(const node* list);

void list_foreach(const node * list, void (* f)(int));

node * list_map(long int (* f)(int), const node * list);

void list_map_mut(int (* f)(int), node * list);

long int list_foldl(int a, long int (* f)(int, int), const node * list);

node * list_iterate(int s, unsigned int n, long int (* f)(int));

#include <stdio.h>
#include <stdbool.h>
#include <sys/resource.h>
#include <limits.h>

#include "linked_list.h"

void print_with_spaces(int a) {
    printf("%d ", a);
}

void print_with_new_line(int a) {
    printf("%d\n", a);
}

long int square(int a) {
    return a * a;
}

long int cube(int a) {
    return a * a * a;
}

long int sum(int a, int b){
    return a+b;
}

long int min(int a, int b){
    return a < b ? a : b;
}

long int max(int a, int b){
    return a > b ? a : b;
}

int abs(int a) {
    if (a < 0) {
        return -a;
    }
    return a;
}

long int mult_two(int a){
    return 2*a;
}

bool save(node* list, const char* filename){
    FILE * f = fopen(filename, "w");

    if (f == NULL) {
        return false;
    }

    while (list){
        if (fprintf(f, "%d ", list_get(list, 0)) <= 0) {
            return false;
        }
        list=list_node_at(list,1);
    }

    return !fclose(f);

}

bool load(node** list, const char* filename){
    FILE * f = fopen(filename, "r");
    int data;

    if (f == NULL) {
        return false;
    }

    while (fscanf(f, "%d ", &data) == 1) {
        list_add_back(data, list);
    }

    if (ferror(f)) {
        return false;
    }

    return !fclose(f);

}

bool serialize(node* list, const char* filename){
    FILE * f = fopen(filename, "wb");

    if (f == NULL) {
        return false;
    }

    while (list){
        if (fprintf(f, "%d ", list_get(list, 0)) <= 0) {
            return false;
        }
        list=list_node_at(list,1);
    }

    return !fclose(f);

}

bool deserialize(node** list, const char* filename){
    FILE * f = fopen(filename, "rb");
    int data;

    if (f == NULL) {
        return false;
    }

    while (fscanf(f, "%d", &data) == 1) {
        list_add_back(data, list);
    }

    if (ferror(f)) {
        return false;
    }

    return !fclose(f);

}

int main() {
    node* list = NULL;
    node* list_squares;
    node* list_cubes;
    int value;

    struct rlimit rlimit;
    getrlimit(RLIMIT_DATA, &rlimit);
    printf("max amount of elements in list:  %lu\n", rlimit.rlim_cur / sizeof(struct node));

    printf("input numbers:");

    while (scanf("%d", &value) == 1) {
        list_add_back(value, &list);
    }

    list_foreach(list, print_with_spaces);
    printf("%c",'\n');
    list_foreach(list, print_with_new_line);
    printf("%c",'\n');

    list_squares = list_map(square, list);
    list_foreach(list_squares, print_with_spaces);
    printf("%c",'\n');


    list_cubes = list_map(cube, list);
    list_foreach(list_cubes, print_with_spaces);
    printf("%c",'\n');
    list_free(list_cubes);

    printf("sum is: %ld\n", list_foldl(0, sum, list));
    printf("min is: %ld\n", list_foldl(INT_MAX, min, list));
    printf("max is: %ld\n", list_foldl(INT_MIN, max, list));

    save(list, "test_save");

    list_map_mut(abs, list);
    list_foreach(list, print_with_spaces);
    printf("%c",'\n');

    load(&list, "test_save");
    list_foreach(list, print_with_spaces);
    printf("%c",'\n');

    serialize(list_squares, "test_serialaize");

    list_squares = list_iterate(1, 10, mult_two);
    list_foreach(list_squares, print_with_spaces);
    printf("%c",'\n');

    deserialize(&list_squares, "test_serialaize");
    list_foreach(list_squares, print_with_spaces);
    printf("%c",'\n');


    return 0;

}



